/*
1) Написать программу по нахождению корня уравнения на промежутке 
с точностью до или по количеству итераций. 
Вывести график функции и точки приближения, количество итераций, значение корня.
Уравнение: x^3-10x^2-3x+59 
Промежуток: [2,4]
Метод: Дихотомии (деления пополам)
2) Осуществить документирование кода и привести к венгерской нотации
*/

// Имплементация класса Уравнение. Содержит коэффициенты уравнения
class CEquation {
	// Имплементация конструктора класса
	constructor(aParams) {
		// Установка аргументов по умолчанию (Параметры указанные в варианте)
		aParams = aParams || {};
		_.defaultsDeep(aParams, {
			m_intKoefX3: ko.observable(1),
			m_intKoefX2: ko.observable(-10),
			m_intKoefX1: ko.observable(-3),
			m_intKoefX0: ko.observable(59),
		})

		// Определение параметров уравнения по полученным аргументам
		this.m_intKoefX3 = aParams.m_intKoefX3;
		this.m_intKoefX2 = aParams.m_intKoefX2;
		this.m_intKoefX1 = aParams.m_intKoefX1;
		this.m_intKoefX0 = aParams.m_intKoefX0;

		// Функция расчета уравнения
		this.Calculate = (x) => this.m_intKoefX3() * Math.pow(x, 3)
			+ this.m_intKoefX2() * Math.pow(x, 2)
			+ this.m_intKoefX1() * x
			+ this.m_intKoefX0();
	}
}

// Имплементация класса промежуток уравнения
class CInterval {
	// Имплементация конструктора класса
	constructor(aParams) {
		// Установка аргументов по умолчанию (Параметры указанные в варианте)
		aParams = aParams || {};
		_.defaultsDeep(aParams, {
			m_intMin: ko.observable(2),
			m_intMax: ko.observable(4),
		})

		// Определение интервала уравнения по полученным аргументам
		this.m_intMin = aParams.m_intMin;
		this.m_intMax = aParams.m_intMax;		
	}
}

// Имплементация класса метода расчета Дихотомией
class CDihotomija {
	// Имплементация конструктора класса
	constructor(aEquation, aInterval, aEpsilon) {
		// Уравнение
		this.m_tEquation = aEquation;
		// Промежуток
		this.m_tInterval = aInterval;
		// Погрешность
		this.m_fltEpsilon = aEpsilon;

		// Массив результатов итераций
		this.m_arrIterations = ko.observable([]);

		// Корень уравнения
		this.m_fltResult = ko.pureComputed(() => {
			let m_intMin = this.m_tInterval.m_intMin();
			let m_intMax = this.m_tInterval.m_intMax();
			let intResult;

			// Обнуление счетчика итераций
			this.m_arrIterations([]);

			// Выполняем метод половинного деления пока отрезок влазит в указанную погрешность
			while (m_intMax - m_intMin > this.m_fltEpsilon()){

				// Делим отрезок пополам
				intResult = (m_intMin + m_intMax) / 2;

				if(this.m_tEquation.Calculate(intResult) >= 0){
					m_intMin = intResult;
				}
				else {
					m_intMax = intResult;
				}

				this.m_arrIterations([...this.m_arrIterations(), intResult]);
			}

			return intResult;
		});
	}
}

// Имплементация класса для представления
class AppViewModel{
	constructor() {
		// переменная-член - уравнение
		this.m_tEquation = new CEquation();
		// переменная-член - промежуток
		this.m_tInterval = new CInterval();
		// переменная-член - погрешность
		this.m_iEpsilon = ko.observable(0.01);
		// переменная-член - метод расчета
		this.m_tDihotomija = new CDihotomija(this.m_tEquation, this.m_tInterval, this.m_iEpsilon);

		// Вызов метода отображения
		this.Draw();

		// Подписка на обновление параметров уравнения с последующиим обновлением отображения графика
		this.m_tEquation.m_intKoefX3.subscribe((v) => this.Draw());
		this.m_tEquation.m_intKoefX2.subscribe((v) => this.Draw());
		this.m_tEquation.m_intKoefX1.subscribe((v) => this.Draw());
		this.m_tEquation.m_intKoefX0.subscribe((v) => this.Draw());

		// Подписка на обновление промежутка уравнения с последующиим обновлением отображения графика
		this.m_tInterval.m_intMin.subscribe((v) => this.Draw());
		this.m_tInterval.m_intMax.subscribe((v) => this.Draw());
	}

	// Метод для отображения графика на экране
	Draw () {
		this.m_tDihotomija.m_fltResult();
		let arrPoints = [];

		// Заполнение массива точками для отображения
		for (let ix = this.m_tInterval.m_intMin(); ix <= this.m_tInterval.m_intMax(); ix++) {
			arrPoints.push(this.m_tEquation.Calculate(ix));
		}

		let intPointsCount = arrPoints.length;
		let arrColumns = [
			[
				'x', 
				..._.range(this.m_tInterval.m_intMin(), 
				this.m_tInterval.m_intMax() + 1),
			],
			['Дихотомия', ...arrPoints],
		];

		for (let ix = 0; ix <= this.m_tDihotomija.m_arrIterations().length - 1; ix++) {
			const pointX = this.m_tDihotomija.m_arrIterations()[ix];
			const pointY = this.m_tEquation.Calculate(pointX);

			console.log( ix + ': {' + pointX + '; ' + pointY + '}');

			arrColumns[0].push(pointX);
			arrColumns.push([ix.toString(), ..._.times(intPointsCount, _.constant(null)), pointY]);

			intPointsCount++;
		}

		c3.generate({
			data: {
				x: 'x',
				columns: arrColumns,
				type: 'spline'
			}
		});
	}
}

// Инициализация модели для представления
var vm = new AppViewModel();
ko.applyBindings(vm);